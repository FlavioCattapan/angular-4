import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { CartItem } from 'app/restaurants-detail/shopping-cart/car-item.model';

@Component({
  selector: 'mt-order-itens',
  templateUrl: './order-itens.component.html'
})
export class OrderItensComponent implements OnInit {

  @Input() itens: CartItem[]
  @Output() increaseQty = new EventEmitter<CartItem>()
  @Output() decreaseQty = new EventEmitter<CartItem>()
  @Output() remove = new EventEmitter<CartItem>()

  constructor() { }

  ngOnInit() {
  }

  // recebe um item para emitir baseado para o item
  emitIncreaseQty(item: CartItem){
    this.increaseQty.emit(item)
  }
  emitDecreaseQty(item: CartItem){
    this.decreaseQty.emit(item)
  }

  emitRemove(item: CartItem){
    this.remove.emit(item)
  }

}
