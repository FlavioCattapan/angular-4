import { Injectable } from "@angular/core";
import { ShoppingCartService } from "app/restaurants-detail/shopping-cart/shopping-cart.service";
import { CartItem } from "app/restaurants-detail/shopping-cart/car-item.model";
import { Observable } from "rxjs/Observable";
import { Http, Headers, RequestOptions } from "@angular/http";
import { MEAT_API } from "app/app.pi";
import { Order } from "./order.model";
// vai ser injetado
@Injectable()
export class OrderService {



    constructor(private cartService: ShoppingCartService, private http: Http) { }

    checkOrder(order: Order): Observable<string> {
        const headers = new Headers()
        headers.append('Content-Type', 'application/json');
        return this.http.post(`${MEAT_API}/orders`,
         
            JSON.stringify(order), new RequestOptions({ headers: headers })).map(response => response.json()).map(order => order.number);


            // pode retornar um order 
            // ou pode retornar um id se vc chamar o map novamente



    }


    carItems(): CartItem[] {
        return this.cartService.items
    }

    itemsValue(): number {
        return this.cartService.total()
    }

    increaseQty(item: CartItem) {
        this.cartService.increaseQty(item);
    }

    decreaseQty(item: CartItem) {
        this.cartService.decreaseQty(item);
    }

    remove(item: CartItem) {
        this.cartService.removeItem(item)
    }

    clear() {
        this.cartService.clear();
    }


}