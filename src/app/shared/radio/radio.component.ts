import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { RadioOption } from './radio-option.model';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'mt-radio',
  templateUrl: './radio.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(()=>RadioComponent),
      multi: true
      
    }
  ]
})
export class RadioComponent implements OnInit, ControlValueAccessor {

  @Input() options: RadioOption[]

  value: any

  onChange: any

  constructor() { }

  ngOnInit() {
  }

  setValue(value: any) {
    this.value = value
    this.onChange(value)
  }
  // ponte para convesar com as diretivas de formularios
  // ControlValueAccessor 4 metodos
  /**
     * Set the function to be called when the control receives a change event.
     */
  // passa um valor para o seu componente
  writeValue(obj: any): void {
    this.value = obj
  }
  /**
   * Set the function to be called when the control receives a touch event.
   */
  // passa um função tem que ser chamada sempre que o valor interno mudar
  // tem que chamar ela no setValue(value: any)
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  /**
   * This function is called when the control status changes to or from "DISABLED".
   * Depending on the value, it will enable or disable the appropriate DOM element.
   *
   * @param isDisabled
   */
  registerOnTouched(fn: any): void { }

  setDisabledState?(isDisabled: boolean): void { }

}
