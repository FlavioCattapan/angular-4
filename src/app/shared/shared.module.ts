import { NgModule, ModuleWithProviders } from "@angular/core";
import { InputComponent } from "./input/input.component";
import { RadioComponent } from "./radio/radio.component";
import { RatingComponent } from "./rating/rating.component";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RestaurantService } from "app/restaurants/restaurants.service";
import { ShoppingCartService } from "app/restaurants-detail/shopping-cart/shopping-cart.service";
import { OrderService } from "app/order/order.service";
import { SnackbarComponent } from "./messages/snackbar/snackbar.component";
import { NotificationService } from "./messages/snackbar/notification.service";

@NgModule({
    declarations:[
        InputComponent,
        RadioComponent,
        RatingComponent,
        SnackbarComponent

    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ], exports: [
        InputComponent,
        RadioComponent,
        RatingComponent, 
        FormsModule, 
        ReactiveFormsModule,
        SnackbarComponent
    ]

})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [RestaurantService, ShoppingCartService, OrderService, NotificationService]
        }
    }
}