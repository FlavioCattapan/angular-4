import { Component, OnInit } from '@angular/core';
import { RestaurantService } from 'app/restaurants/restaurants.service';
import { Restaurant } from 'app/restaurants/restaurant/restaurant.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'mt-restaurants-detail',
  templateUrl: './restaurants-detail.component.html'
})
export class RestaurantsDetailComponent implements OnInit {

  restaurant: Restaurant

  constructor(private restauranstsService: RestaurantService, private route: ActivatedRoute) { }

  ngOnInit() {

    let id:string = this.route.snapshot.params['id'];

   this.restauranstsService.restaurantById(id).subscribe(
     restaurant => {
       this.restaurant = restaurant
       console.log(restaurant);
      }
   );


  }

}
