import { Component, OnInit } from '@angular/core';
import { Restaurant } from './restaurant/restaurant.model';
import { RestaurantService } from './restaurants.service';
import {trigger, state, style, transition, animate} from '@angular/animations'
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import 'rxjs/add/operator/switchMap'

@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html',
  animations: [
    trigger('toggleSearch', [
      state('hidden', style({
        opacity: 0,
        "max-height": "0px"
      })),
      state('visible', style({
        opacity: 1,
        "max-height": "70px",
        "margin-top": "20px"
      })),
      transition('* => *', animate('250ms 0s ease-in-out'))
    ])
  ]
})
export class RestaurantsComponent implements OnInit {

  restaurants: Restaurant[]

  serachBarState = 'hidden'

  searchForm: FormGroup
  searchControl: FormControl

  constructor(private restaurantService: RestaurantService,
    private fb: FormBuilder) {

   }

  //  this.searchForm = this.fb.group({

  //  })

  ngOnInit() {
    this.restaurantService.restaurants().switchMap( restaurants => this.restaurants = restaurants)

    this.searchControl = this.fb.control('')
    this.searchForm = this.fb.group({
      searchControl: this.searchControl
    })

    this.searchControl.valueChanges.subscribe(
      searchTerm => console.log(searchTerm)
    )

    this.restaurantService.restaurants().subscribe(restaurants =>
      this.restaurants = restaurants)
  }

  toggleSearch(){
  this.serachBarState = this.serachBarState === 'hidden' ? 'visible' : 'hidden';
  }

}
