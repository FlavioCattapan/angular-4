import { NgModule } from "@angular/core";
import { RestaurantService } from "app/restaurants/restaurants.service";
import { ShoppingCartService } from "app/restaurants-detail/shopping-cart/shopping-cart.service";
import { OrderService } from "app/order/order.service";

@NgModule({
    providers: [
        RestaurantService, ShoppingCartService, OrderService
    ]
})

export class CoreModule{}